import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../style/Section.scss";

export class SectionComponent extends Component {
  render() {
    return (
      <div className="section-container">
        <div className="category">
          <h4 className="section">Browse by category</h4>
          <ul className="section-list">
            <li>all </li>
            <li>anime</li>
            <li>action</li>
            <li>adventure</li>
            <li>science fiction</li>
            <li>comedy</li>
          </ul>
        </div>
        <div className="wrapper">
          <div className="card">
            <Link to="/detailpage">
              <img
                className="img-card"
                src="https://m.media-amazon.com/images/M/MV5BMDIxNTdhYzUtNGY3My00MTljLTlmNTctODA5MjAzN2JlN2Y3XkEyXkFqcGdeQXVyMTgyOTQ3ODM@._V1_SY1000_CR0,0,671,1000_AL_.jpg"
                alt="/"
              />
            </Link>
            <span>Merah putih memanggil</span> <br/>
            <span>2014</span>
          </div>
          <div className="card">
            <Link to="/detailpage">
              <img
                className="img-card"
                src="https://4.bp.blogspot.com/-gBFDbWlqWmw/USHGV2lmSjI/AAAAAAAAAAM/47ZQNS8j5cI/s1600/VERTICAL+LIMIT.jpg"
                alt="/"
              />
            </Link>
          </div>
          <div className="card">
            <Link to="/detailpage">
              <img
                className="img-card"
                src="https://m.media-amazon.com/images/M/MV5BMDIxNTdhYzUtNGY3My00MTljLTlmNTctODA5MjAzN2JlN2Y3XkEyXkFqcGdeQXVyMTgyOTQ3ODM@._V1_SY1000_CR0,0,671,1000_AL_.jpg"
                alt="/"
              />
            </Link>
          </div>
          <div className="card">
            <Link to="/detailpage">
              <img
                className="img-card"
                src="https://m.media-amazon.com/images/M/MV5BMDIxNTdhYzUtNGY3My00MTljLTlmNTctODA5MjAzN2JlN2Y3XkEyXkFqcGdeQXVyMTgyOTQ3ODM@._V1_SY1000_CR0,0,671,1000_AL_.jpg"
                alt="/"
              />
            </Link>
          </div>
          <div className="card">
            <Link to="/detailpage">
              <img
                className="img-card"
                src="https://m.media-amazon.com/images/M/MV5BMDIxNTdhYzUtNGY3My00MTljLTlmNTctODA5MjAzN2JlN2Y3XkEyXkFqcGdeQXVyMTgyOTQ3ODM@._V1_SY1000_CR0,0,671,1000_AL_.jpg"
                alt="/"
              />
            </Link>
          </div>
          <div className="card">
            <Link to="/detailpage">
              <img
                className="img-card"
                src="https://m.media-amazon.com/images/M/MV5BMDIxNTdhYzUtNGY3My00MTljLTlmNTctODA5MjAzN2JlN2Y3XkEyXkFqcGdeQXVyMTgyOTQ3ODM@._V1_SY1000_CR0,0,671,1000_AL_.jpg"
                alt="/"
              />
            </Link>
          </div>
          <div className="card">
            <Link to="/detailpage">
              <img
                className="img-card"
                src="https://m.media-amazon.com/images/M/MV5BMDIxNTdhYzUtNGY3My00MTljLTlmNTctODA5MjAzN2JlN2Y3XkEyXkFqcGdeQXVyMTgyOTQ3ODM@._V1_SY1000_CR0,0,671,1000_AL_.jpg"
                alt="/"
              />
            </Link>
          </div>
          <div className="card">
            <Link to="/detailpage">
              <img
                className="img-card"
                src="https://m.media-amazon.com/images/M/MV5BMDIxNTdhYzUtNGY3My00MTljLTlmNTctODA5MjAzN2JlN2Y3XkEyXkFqcGdeQXVyMTgyOTQ3ODM@._V1_SY1000_CR0,0,671,1000_AL_.jpg"
                alt="/"
              />
            </Link>
          </div>
          <div className="card">
            <Link to="/detailpage">
              <img
                className="img-card"
                src="https://m.media-amazon.com/images/M/MV5BMDIxNTdhYzUtNGY3My00MTljLTlmNTctODA5MjAzN2JlN2Y3XkEyXkFqcGdeQXVyMTgyOTQ3ODM@._V1_SY1000_CR0,0,671,1000_AL_.jpg"
                alt="/"
              />
            </Link>
          </div>
          <div className="card">
            <Link to="/detailpage">
              <img
                className="img-card"
                src="https://m.media-amazon.com/images/M/MV5BMDIxNTdhYzUtNGY3My00MTljLTlmNTctODA5MjAzN2JlN2Y3XkEyXkFqcGdeQXVyMTgyOTQ3ODM@._V1_SY1000_CR0,0,671,1000_AL_.jpg"
                alt="/"
              />
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default SectionComponent;
